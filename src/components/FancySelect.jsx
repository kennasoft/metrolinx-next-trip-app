import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import RSelect from 'react-select';
import { fetchDirections, fetchStops, fetchPrediction, selectRoute, selectDirection, selectStop, resetPrediction } from '../actions';
import { PENDING } from '../actions/constants';

import '../assets/styles/fancy-select.css';
import 'react-select/dist/react-select.css';

//this component uses `react-select` to render a nicer drop-down menu that the html <select> element
class FancySelect extends Component {

  constructor(props){
    super(props);
    this.state = { 
      disabled: false,
			searchable: true,
			selectValue: props.selectedValue,
      clearable: true,
      loading: props.loading
     };
    this.props = props;
    this.updateValue = this.updateValue.bind(this);
  }

  componentDidUpdate(prevProps, prevState){
    //this resets the drop-down value when a parent drop-down changes
    if(this.props.selectedValue===null && prevProps.selectedValue!==null){
      this.setState({selectValue: null});
    }
  }

  updateValue(newValue) {
    //update component state
    this.setState({
      selectValue: newValue
    });
    if(newValue){
      //dispatch action to load values into next drop-down
      this.props.triggerNext(newValue.value);
      //dispatch action to save the selected value in global state
      this.props.setSelected(newValue);
    }
  } 

  render() {
    const { type, data } = this.props;
    // console.log(data);
    const options = data || [] ; //(new Array(20)).fill(1,0).map((v, i) => ({value: i, label: `${type} ${i}`}));
    return (
      <RSelect
          id={ `${type}-select` }
          placeholder={`Select ${type}`}
					ref={ (ref) => { this.select = ref; } }
					onBlurResetsInput={false}
					onSelectResetsInput={false}
					autoFocus
					options={options}
					clearable={this.state.clearable}
					name={`selected-${type}`}
          disabled={this.state.disabled}
					value={this.state.selectValue}
					onChange={this.updateValue}
          searchable={this.state.searchable}
          isLoading={this.state.loading}
				/>
    );
  }
}

FancySelect.propTypes = {
  type: PropTypes.string
}

const mapStateToProps = (state, ownProps) => {
  return {
    ...state,
    ...ownProps,
    selectedValue: state.selections[ownProps.type.toLowerCase()],
    loading: state[`${ownProps.type}s`] && state[`${ownProps.type}s`].requestStatus === PENDING,
    data: ownProps.data || state[`${ownProps.type}s`].values
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  switch(ownProps.type.toLowerCase()){
    case 'route': return {
      triggerNext: route => {dispatch(resetPrediction()); return dispatch(fetchDirections(route.tag))},
      setSelected: item => dispatch(selectRoute(item))
    }
    case 'direction': return {
      triggerNext: direction => {dispatch(resetPrediction()); return dispatch(fetchStops(direction))},
      setSelected: item => dispatch(selectDirection(item))
    }
    case 'stop': return {
      triggerNext: (stop) => dispatch(fetchPrediction(stop.routeTag, stop.tag)),
      setSelected: item => dispatch(selectStop(item))
    }
    default: return {};
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(FancySelect);
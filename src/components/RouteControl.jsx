import React, { Component } from 'react';
import { connect } from 'react-redux';

import { fetchRoutes } from '../actions';
import FancySelect from './FancySelect';

import routeIcon from '../assets/route.png';
import directionIcon from '../assets/direction.png';
import stopIcon from '../assets/stop.png';

import '../assets/styles/route-control.css';

//this represents the boxes in the UI that contain the icons and drop-downs
class RouteControl extends Component {

  constructor(){
    super();
    this.state = {
      icons: {
        route: routeIcon,
        direction: directionIcon,
        stop: stopIcon
      }
    };
  }

  componentDidMount(){
    this.props.fetchRoutes();
  }

  render() {
    const { type } = this.props;
    const icon = this.state.icons[type.toLowerCase()];
    return (
      <label className="route-control flex-item">
        <img src={icon} alt={`${type} Icon`} />
        <FancySelect {...this.props} />
      </label>
    );
  }
}

//this maps global state to the relevant props for the target component, based on the `type` prop
const mapStateToProps = (state, ownProps) => {
  switch(ownProps.type.toLowerCase()){
    case 'route': return {
      ...ownProps,
      ...state,
      data: state.routes.values
    }
    case 'direction': return {
      ...ownProps,
      ...state,
      data: state.directions.values
    }    
    case 'stop': return {
      ...ownProps,
      ...state,
      data: state.stops.values
    }
    default: return { ...ownProps, ...state }
  }
}

//add the call to fetch routes to props to make it accessible to the component
const mapDispatchToProps = (dispatch) => {
    return {
        fetchRoutes: () => dispatch(fetchRoutes())
    }
};

//connect the react component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(RouteControl);
import { FETCH_ROUTES, PENDING, FULFILLED, REJECTED } from '../actions/constants';

const initialState = {
  values: [],
  requestStatus: null,
  error: null
}

export default (state = initialState, action) => {

  switch (action.type) {
    case `${FETCH_ROUTES}_${PENDING}`:  return { ...state, requestStatus: PENDING }; 
    case `${FETCH_ROUTES}_${FULFILLED}`:
      return {
        ...state,
        //return a list of (value => label) pairs, for compatibility with the fancy select component
        values: action.payload.route.map(route => ({value: route, label: route.title})),
        requestStatus: FULFILLED
      }; 
    case `${FETCH_ROUTES}_${REJECTED}`:
      return {
        ...state,
        error: action.payload['Error'],
        requestStatus: REJECTED
      };
    default: return state;
  }

}
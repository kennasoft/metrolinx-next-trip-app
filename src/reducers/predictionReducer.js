import { FETCH_PREDICTION, PENDING, FULFILLED, REJECTED, RESET_PREDICTION } from '../actions/constants';

const initialState = {
  prediction: {},
  requestStatus: null,
  error: null
}

export default (state = initialState, action) => {

  switch (action.type) {
    case `${FETCH_PREDICTION}_${PENDING}`:
      return {
        ...state,
        requestStatus: PENDING
      };
    case `${FETCH_PREDICTION}_${FULFILLED}`:
      return {
        ...state,
        prediction: action.payload.predictions,
        requestStatus: FULFILLED
      }
    case `${FETCH_PREDICTION}_${REJECTED}`: 
      return {
        ...state,
        error: action.payload['Error'],
        requestStatus: REJECTED
      }
    case RESET_PREDICTION: 
      return {
        ...state,
        prediction: {} 
      }
    default: return state;
  }

}
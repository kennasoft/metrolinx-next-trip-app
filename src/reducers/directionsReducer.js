import { FETCH_DIRECTIONS, PENDING, FULFILLED, REJECTED } from '../actions/constants';

const initialState = {
  values: [],
  requestStatus: null,
  error: null
}

export default (state = initialState, action) => {

  switch (action.type) {
    case `${FETCH_DIRECTIONS}_${PENDING}`: 
      return {
        ...state,
        requestStatus: PENDING
      }
    case `${FETCH_DIRECTIONS}_${FULFILLED}`: 
      return {
        ...state,
        values: action.payload.route.direction.map(dir => {
          //return a list of (value => label) pairs, for compatibility with the fancy select component
          return {
            value: {
              ...dir,
              route: action.payload.route
            },
            label: dir.title
          }
        }),
        requestStatus: FULFILLED
      }
    case `${FETCH_DIRECTIONS}_${REJECTED}`: 
      return {
        ...state,
        error: action.payload['Error'],
        requestStatus: REJECTED
      }
    default: return state;
  }

}
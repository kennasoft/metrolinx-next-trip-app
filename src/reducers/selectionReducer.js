import { SELECT_DIRECTION, SELECT_ROUTE, SELECT_STOP } from '../actions/constants';

const initialState = {
  route: null,
  direction: null,
  stop: null
}

export default (state = initialState, action) => {

  switch (action.type) {
    case SELECT_ROUTE: 
      return {
        ...state,
        route: action.payload || null,
        direction: null,
        stop: null
      }
    case SELECT_DIRECTION: 
      return {
        ...state,
        direction: action.payload || null,
        stop: null
      }
    case SELECT_STOP:
      return {
        ...state,
        stop: action.payload || null
      }
    default: return state;
  }

}
import { combineReducers } from 'redux';
import routesReducer from './routesReducer';
import directionsReducer from './directionsReducer';
import stopsReducer from './stopsReducer';
import predictionReducer from './predictionReducer';
import selectionReducer from './selectionReducer';

export default combineReducers({ 
    routes: routesReducer,
    directions: directionsReducer,
    stops: stopsReducer,
    prediction: predictionReducer,
    selections: selectionReducer
});
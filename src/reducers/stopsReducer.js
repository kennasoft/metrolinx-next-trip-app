import { FETCH_STOPS, FULFILLED } from '../actions/constants';

const initialState = {
  values: [],
  requestStatus: null,
  error: null
}

export default (state = initialState, action) => {

  switch (action.type) {
    case `${FETCH_STOPS}`: 
      return {
        ...state,
        //return a list of (value => label) pairs, for compatibility with the fancy select component
        values: action.payload.map(stop => ({ value: stop, label: stop.title })) || [],
        requestStatus: FULFILLED
      }
    default: return state;
  }

}
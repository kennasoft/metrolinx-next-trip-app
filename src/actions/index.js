import { FETCH_ROUTES, FETCH_DIRECTIONS, FETCH_STOPS, FETCH_PREDICTION, TRIP_API_BASE_URL, SELECT_ROUTE, SELECT_DIRECTION, SELECT_STOP, RESET_PREDICTION } from './constants';
import axios from 'axios';

//api call to fetch all routes
export const fetchRoutes = () => {
  return {
    type: FETCH_ROUTES,
    payload: axios.get(`${TRIP_API_BASE_URL}routeList`).then(resp => resp.data)
  }
}

//api call to fetch all routeConfigs
export const fetchDirections = (route) => {
  return {
    type: FETCH_DIRECTIONS,
    payload: axios.get(`${TRIP_API_BASE_URL}routeConfig&r=${route}`).then(resp => resp.data)
  }
}

//this is not an async operation. it simply extracts relevant info from the direction object
export const fetchStops = (direction) => {
  return {
    type: FETCH_STOPS,
    payload: direction.route.stop
      .filter( item => direction.stop.map(s => s.tag).indexOf(item.tag) > -1 )
      .map(stop => ({ ...stop, routeTag: direction.route.tag }))
  }
}

//api call to fetch final result based on previous selections
export const fetchPrediction = (route, stop) => {
  return {
    type: FETCH_PREDICTION,
    payload: axios.get(`${TRIP_API_BASE_URL}predictions&r=${route}&s=${stop}`).then(resp => resp.data)
  }
}

//the actions below are not asynchronous, but keep the state of selected options
export const selectRoute = (route) => {
  return {
    type: SELECT_ROUTE,
    payload: route
  }
}

export const selectDirection = (direction) => {
  return {
    type: SELECT_DIRECTION,
    payload: direction
  }
}

export const selectStop = (stop) => {
  return {
    type: SELECT_STOP,
    payload: stop
  }
}

//this action simply resets the final result to empty object
export const resetPrediction = () => {
  return {
    type: RESET_PREDICTION,
    payload: null
  }
}

//export all actions again as a single object, so that we can do "import actions from './actions';""
export default {
  fetchRoutes,
  fetchDirections,
  fetchStops,
  fetchPrediction,
  selectRoute,
  selectDirection,
  selectStop,
  resetPrediction
}

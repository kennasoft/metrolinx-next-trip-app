//having all the action constants defined in a single file to guard against naming clashes
//instead of the namespaced action names like 'NAMESPACE/FETCH_ROUTES'

export const FETCH_ROUTES = 'FETCH_ROUTES';
export const FETCH_DIRECTIONS = 'FETCH_DIRECTIONS';
export const FETCH_STOPS = 'FETCH_STOPS';
export const FETCH_PREDICTION = 'FETCH_PREDICTION';

export const SELECT_ROUTE = 'SELECT_ROUTE';
export const SELECT_DIRECTION = 'SELECT_DIRECTION';
export const SELECT_STOP = 'SELECT_STOP';

export const RESET_PREDICTION = 'RESET_PREDICTION';

export const PENDING = 'PENDING';
export const FULFILLED = 'FULFILLED';
export const REJECTED = 'REJECTED';

export const TRIP_API_BASE_URL = 'http://webservices.nextbus.com/service/publicJSONFeed?a=ttc&command=';

export default {
    FETCH_ROUTES,
    FETCH_DIRECTIONS,
    FETCH_STOPS,

    SELECT_ROUTE,
    SELECT_DIRECTION,
    SELECT_STOP,

    RESET_PREDICTION,

    PENDING,
    FULFILLED,
    REJECTED,

    TRIP_API_BASE_URL
}

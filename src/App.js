import React, { Component } from 'react';
import { connect } from 'react-redux';
import {ModalContainer, ModalDialog} from 'react-modal-dialog';

import logo from './assets/metrolinx-logo.png';
import clockIcon from './assets/clock-icon.png';

import RouteControl from './components/RouteControl';

import './assets/styles/base.css';
import './assets/styles/App.css';


class App extends Component {

  constructor(props){
    super();
    this.props = props;
    this.state = {
      nextTrip: this.computeNextTrip(),
      prediction: this.props.prediction,
      isShowingModal: false,
      isShowingErrorModal: false
    }
    this.computeNextTrip = this.computeNextTrip.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }

  componentDidUpdate(prevProps, prevState){
    if(this.props.prediction && prevProps !== this.props){
      this.setState({nextTrip: this.computeNextTrip()});
    }
  }

  computeNextTrip(){
    if(!this.props.prediction.direction) {
      return {};
    } else if(this.props.prediction.dirTitleBecauseNoPredictions){
      this.setState({isShowingErrorModal: true});
      return {};
    }
    const nextTrip = this.props.prediction.direction.prediction
      .map(pred => {
        return {title: this.props.prediction.direction.title, seconds: pred.seconds};
      })
      .reduce((prev, curr) => {
        //compare predictions for the closest one
        return (curr.seconds - prev.seconds < 0) ? curr : prev;
      }, {title:'', seconds: 1000000});
    this.setState({ isShowingModal: true });
    nextTrip.description = nextTrip.title.split(' - ')[0] + ' to ' + nextTrip.title.substring(nextTrip.title.indexOf(' - ')+3);
    return nextTrip;
  }

  handleClose(){
    this.setState({isShowingModal: false, isShowingErrorModal: false});
  }

  render() {

    return (
      <section>
        <header>
          <h1><img src={logo} alt="Metrolinx Logo" /></h1>
          <h2 className="app-title">Your Next Trip</h2>
        </header>
        <div className="container flex">
          <RouteControl type={'Route'} />
          <RouteControl type={'Direction'} />
          <RouteControl type={'Stop'} />
        </div>
        {
          this.state.isShowingModal &&
          <ModalContainer onClose={this.handleClose}>
            <ModalDialog onClose={this.handleClose}>
              <h2>Time before next trip
              going {this.state.nextTrip.description}</h2>
              <div className="center-text">
                <img src={clockIcon} alt="Clock Icon" />
                {
                  <h3>{ `${parseInt(this.state.nextTrip.seconds/60, 10)} minute(s) and ${this.state.nextTrip.seconds % 60} second(s)` }</h3>
                }
              </div>
            </ModalDialog>
          </ModalContainer>
        }
        {
          this.state.isShowingErrorModal && 
          <ModalContainer onClose={this.handleClose}>
            <ModalDialog onClose={this.handleClose}>
              <h3>Sorry, we are unable to get trip schedules at the moment</h3>
              <div className="center-text">
                <img src={clockIcon} alt="Clock Icon" />
                <h3>please try later</h3>
              </div>
            </ModalDialog>
          </ModalContainer>
        }
      </section>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    ...state,
    prediction: state.prediction.prediction
  }
}


export default connect(mapStateToProps)(App);

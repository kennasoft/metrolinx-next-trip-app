# Metrolinx Next Trip App

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## Instructions on how to run this project

## Prerequisites
 * You need to install node.js and npm on your machine to be able to run this application.

## Running on local
 * clone the repo from [here](git@bitbucket.org:kennasoft/metrolinx-next-trip-app.git).
 * run `npm install`
 * then run `npm start`
 * the app should launch at `http://localhost:3000`

You can try it out by visiting that url in your browser.

NB: has been tested on IE 11 and Chrome 66.

##Screenshots

1. Desktop

![Screenshot](https://bitbucket.org/kennasoft/metrolinx-next-trip-app/raw/master/Metrolinx%20Next%20Trip%20App%20-%20Desktop.png)

2. Mobile

![Screenshot](https://bitbucket.org/kennasoft/metrolinx-next-trip-app/raw/master/Metrolinx%20Next%20Trip%20App%20-%20Mobile.png)

3. Tablet

![Screenshot](https://bitbucket.org/kennasoft/metrolinx-next-trip-app/raw/master/Metrolinx%20Next%20Trip%20App%20-%20Tablet.png)